#include <stdio.h>

/* echo command-line arguments; 1st version */
int main(int argc, char *argv[])
{
  for (size_t i = 0; i < argc; i++)
      {
	// last argument is to put spaces between output but not after last one
        printf("%s%s", argv[i], (i != argc - 1) ? " " : "");
      }
    printf("\n");
    return 0;
}
